const WEATHER_API_KEY = "c41f33018e84542a327f8040a2cc70e9";
const API_URL = "https://api.openweathermap.org/data/2.5/weather?";

function cityUrl(city) {
  return `${API_URL}q=${city}&ybuts=metric&APPID=${WEATHER_API_KEY}`;
}

export const fetchweatherByCity = async city => {
  const response = await fetch(cityUrl(city));
  const {name, weather, main} = await response.json();
  const {main : weather_group} = weather[0];
  const {temp} = main;

  return {location: name, weather: weather_group, temperature: temp};
};
export const fetchLocationId = async city => {
  const response = await fetch(
    `https://www.metaweather.com/api/location/search/?query=${city}`,
  );
  const locations = await response.json();
  return locations[0].woeid;
};

export const fetchWeather = async woeid => {
  const response = await fetch(
    `https://www.metaweather.com/api/location/${woeid}/`,
  );
  const { title, consolidated_weather } = await response.json();
  const { weather_state_name, the_temp } = consolidated_weather[0];

  return {
    location: title,
    weather: weather_state_name,
    temperature: the_temp,
  };
};
